package id.or.jisedu.login

import id.or.jisedu.errorAware
import id.or.jisedu.get
import id.or.jisedu.user.UserController
import io.javalin.Context

object LoginController {
	fun handleLoginPost(ctx: Context) {
		ctx.errorAware {
			val username = ctx["username"]?.trim() ?: throw IllegalArgumentException("Username not provided")
			val password = ctx["password"] ?: throw IllegalArgumentException("Password not provided")
			val user = UserController.authenticate(username, password)
			if (user == null) {
				ctx.renderVelocity("velocity/login/login.vm", mutableMapOf("auth" to false))
				return@errorAware
			}
			
			ctx.sessionAttribute("auth", true)
			ctx.sessionAttribute("currentUser", user)
			val redirect = ctx.sessionAttribute<String?>("redirect")
			ctx.sessionAttribute("redirect", null)
			ctx.redirect(redirect ?: "/")
		}
	}
}