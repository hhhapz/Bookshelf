package id.or.jisedu.user

import java.util.stream.Collectors


object UserDao {
	val users = mutableListOf<User>()
	
	fun userByUsername(username: String) = users.find { it.username == username }
	
	fun getAllUserNames() = users.stream().map { (username) -> username }.collect(Collectors.toList<String>())
	
}

internal data class UserWrapper(val users: MutableList<User>)