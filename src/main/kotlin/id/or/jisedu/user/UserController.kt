package id.or.jisedu.user

import org.mindrot.jbcrypt.BCrypt

object UserController {
	fun authenticate(username: String, password: String): User? {
		val user = UserDao.userByUsername(username) ?: return null
		val hashedPass = BCrypt.hashpw(password, user.salt)
		return if (hashedPass == user.hashedPassword) user else null
	}
	
	fun addAccount(username: String, password: String) {
		val salt = BCrypt.gensalt()
		val hashedPass = BCrypt.hashpw(salt, password)
		UserDao.users.add(User(username, salt, hashedPass))
	}
	
	fun setPassword(username: String, newPass: String) {
		val salt = BCrypt.gensalt()
		
	}
}