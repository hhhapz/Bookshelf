package id.or.jisedu.user

data class User(val username: String, val salt: String, val hashedPassword: String, val admin: Boolean = false)