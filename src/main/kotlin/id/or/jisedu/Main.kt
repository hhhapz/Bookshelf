package id.or.jisedu

import java.util.concurrent.*

fun main(args: Array<String>) {
  println("""
  		a
  		b
  		c
  		d
  		e
  	""".lines().joinToString(", "))
  
}

private fun getFuture() = CompletableFuture<String>().apply {
  Executors.newSingleThreadScheduledExecutor().schedule({ this.complete("Hello World!") }, 20, TimeUnit.SECONDS)
}