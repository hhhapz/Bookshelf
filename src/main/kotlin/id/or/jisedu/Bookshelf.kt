package id.or.jisedu

import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.exc.MismatchedInputException
import com.fasterxml.jackson.module.kotlin.KotlinModule
import id.or.jisedu.Bookshelf.mapper
import id.or.jisedu.Bookshelf.reader
import id.or.jisedu.Bookshelf.writer
import id.or.jisedu.login.LoginController
import id.or.jisedu.post.manual
import id.or.jisedu.post.newAuto
import id.or.jisedu.repo.BookBridge
import id.or.jisedu.repo.BookRepo
import id.or.jisedu.user.*
import io.javalin.Javalin
import io.javalin.event.EventType
import org.mindrot.jbcrypt.BCrypt
import java.io.File

internal object Bookshelf {
	val mapper = ObjectMapper()
	lateinit var writer: ObjectWriter
	lateinit var reader: ObjectReader
}

fun main(args: Array<String>) {
	val file = File("book.json")
	val userFile = File("passwords.json")
	
	if (!file.exists()) file.createNewFile()
	if (!userFile.exists()) userFile.createNewFile()
	
	mapper.registerModule(KotlinModule())
	Bookshelf.writer = mapper.writerWithDefaultPrettyPrinter()
	Bookshelf.reader = mapper.reader()
	
	fun save() {
		try {
			writer.writeValue(file.writer(), BookBridge(BookRepo.getAll()))
			writer.writeValue(userFile.writer(), UserDao.users)
		} catch (e: Exception) {
			println("Exception occured while saving: ${e.message}")
		}
	}
	
	val app = Javalin.create().apply {
		port(7070)
		enableStaticFiles("/public")
	}
	
	app.get("/new") { ctx ->
		if (ctx.sessionAttribute<Boolean?>("auth") == true) new(ctx)
		else {
			ctx.sessionAttribute("redirect", "/new")
			ctx.redirect("/login")
		}
	}
	
	app.post("/login") { ctx -> LoginController.handleLoginPost(ctx) }
	
	app.post("/new-auto") { ctx ->
		if (ctx.sessionAttribute<Boolean?>("auth") == true) {
			newAuto(ctx, args.component1())
			save()
		} else {
			ctx.sessionAttribute("redirect", "/new")
			ctx.redirect("/login")
		}
	}
	
	app.post("/new-book-manual") { ctx ->
		if (ctx.sessionAttribute<Boolean?>("auth") == true) {
			manual(ctx)
			save()
		} else {
			ctx.sessionAttribute("redirect", "/new")
			ctx.redirect("/login")
		}
		
	}
	
	app.get("/books/:id/:amount") { ctx -> updateAmount(ctx) }
	
	app.get("/books/:id") { book(it) }
	
	app.get("/login") { ctx ->
		if (ctx.sessionAttribute<Boolean?>("auth") == true) {
			ctx.redirect("/books")
		} else {
			ctx.renderVelocity("velocity/login/login.vm", mutableMapOf())
		}
		
	}
	
	app.get("/settings") { ctx -> ctx.html("Coming soon") }

//	app.get("/settings") { ctx -> ctx.html("Coming soon") }
	
	app.get("/logout") { ctx ->
		ctx.sessionAttribute("auth", null)
		ctx.sessionAttribute("currentUser", null)
		ctx.redirect("/")
	}
	
	app.get("/books") { ctx -> getAllBooks(ctx, 30) }
	
	app.get("/") { it.redirect("/books") }
	
	app.event(EventType.SERVER_STARTED) { e ->
		try {
			val bridge: BookBridge = reader.forType(BookBridge::class.java).readValue(file)
			bridge.books.forEach(BookRepo::add)
		} catch (ex: RuntimeException) {
			println("Book storage file was found empty. If it shouldn't, please review for valid JSON.")
			writer.writeValue(file.writer(), BookBridge(listOf()))
		}
		
		
		val users: UserWrapper = try {
			reader.forType(UserWrapper::class.java).readValue(userFile)
		} catch (ex: MismatchedInputException) {
			val salt = BCrypt.gensalt()
			val hashedPassword = BCrypt.hashpw("5iveL!fe", salt)
			val salt1 = BCrypt.gensalt()
			val hashedPassword1 = BCrypt.hashpw("password", salt1)
			UserWrapper(mutableListOf(
				User("admin", salt, hashedPassword, true),
				User("embassy", salt1, hashedPassword1)))
		}
		
		UserDao.users.addAll(users.users)
		
		save()
	}
	
	app.start()
}
