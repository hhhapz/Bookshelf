package id.or.jisedu.repo

import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.atomic.AtomicLong

object BookRepo {
	private val id = AtomicLong()
	private val books = CopyOnWriteArrayList<Book>()
	
	fun getAll() = books.toList()
	
	fun add(b: Book) {
		if (books.any { it.title == b.title && it.authors == b.authors && it.edition == b.edition && it.binding == b.binding }) {
			val book = books.find {
				it.title == b.title && it.authors == b.authors && it.edition == b.edition && it.binding == b.binding
			}!!
			book.amount += 1
		} else {
			b.id = id.incrementAndGet()
			books.add(b)
		}
		
	}
	
	fun get(id: String) = books.find { it.id.toString() == id } ?: throw IllegalArgumentException("Id not found")
	
	fun get(id: Long) = books.find { it.id == id } ?: throw IllegalArgumentException("Id not found")
	
	fun getIndex(id: Int) = books[id]
	
	fun size() = books.size
	
	
	fun remove(b: Book) {
		books.remove(b)
	}
	
	fun remove(id: Long) {
		books.remove(get(id))
	}
	
	fun remove(id: String) {
		books.remove(get(id))
	}
	
	fun clear() = books.clear()
	
}