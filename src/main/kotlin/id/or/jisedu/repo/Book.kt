package id.or.jisedu.repo

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

data class BookBridge(val books: List<Book>)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class BookWrapper(@JsonProperty("book") val book: Book)

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Book(
  val title: String,
  @JsonProperty("title_long")
  val titleLong: String? = null,
  val isbn: String? = null,
  val isbn13: String? = null,
  val dewey_decimal: String? = null,
  val format: String? = null,
  val publisher: String? = null,
  val language: String? = null,
  @JsonProperty("date_published")
  val datePublished: String? = null,
  @JsonProperty("publish_date")
  val publishDate: String? = null,
  val edition: String? = null,
  val pages: String? = null,
  val binding: String? = null,
  val dimensions: String? = null,
  val overview: String? = null,
  val excerpt: String? = null,
  val synopsys: String? = null,
  val image: String? = null,
  val authors: List<String>? = mutableListOf("    "),
  val subjects: List<String>? = null,
  var id: Long? = null,
  var amount: Int = 1
)