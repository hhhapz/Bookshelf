package id.or.jisedu

import io.javalin.Context

fun <R> Context.errorAware(message: String? = null, block: () -> R): R? {
	return try {
		block()
	} catch (e: Exception) {
		status(400)
		e.printStackTrace()
		html(message
			     ?: "Error! ${e.message}"
			     ?: "Error! Message not provided.")
		null
	}
}

fun <R> Context.errorAwareISBN(message: String? = null, block: () -> R): Any? {
	return try {
		block()
		null
	} catch (e: Exception) {
		e.message
	}
}

fun String.fixCase(): String {
	val tmp = mutableListOf<String>()
	split(" ").forEach { tmp.add(it.toLowerCase().capitalize()) }
	return tmp.joinToString(" ")
}

operator fun Context.get(name: String): String? = formParam(name).nullIfBlank()

fun String?.nullIfBlank() = if (this?.isEmpty() != false) null else this

fun String.isValidISBN(): Boolean {
	val isbn = replace("-", "")
	return when {
		isbn.length == 10 -> {
			true
		}
		isbn.length == 13 -> {
			
			var tot = 0
			for (i in 0..11) {
				val digit = isbn.substring(i, i + 1).toIntOrNull()
					?: return false
				tot += if (i % 2 == 0) digit * 1 else digit * 3
			}
			var checksum = 10 - tot % 10
			if (checksum == 10) checksum = 0
			checksum == isbn.substring(12).toIntOrNull()
		}
		else              -> false
	}
}

fun String.containsIgnoreCase(anotherString: String) = this.toLowerCase().contains(anotherString.toLowerCase())

