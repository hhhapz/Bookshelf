package id.or.jisedu.post

import id.or.jisedu.*
import id.or.jisedu.repo.Book
import id.or.jisedu.repo.BookRepo
import io.javalin.Context

fun manual(ctx: Context) {
	ctx.errorAware {
		
		val title = ctx["title"]?.fixCase() ?: throw IllegalArgumentException("Title was not provided")
		val titleLong = ctx["title_long"]?.fixCase()
		val isbn = ctx["isbn"]?.fixCase()
		val isbn13 = ctx["isbn13"]?.fixCase()
		val deweyDecimal = ctx["dewey_decimal"]?.fixCase()
		val format = ctx["format"]?.fixCase()
		val publisher = ctx["publisher"]?.fixCase()
		val language = ctx["language"]?.fixCase()
		val datePublished = ctx["date_published"]?.fixCase()
		val edition = ctx["edition"]?.fixCase()
		val pages = ctx["pages"]?.fixCase()
		val binding = ctx["binding"]?.fixCase()
		val dimensions = ctx["dimensions"]?.fixCase()
		val overview = ctx["overview"]?.fixCase()
		val excerpt = ctx["excerpt"]?.fixCase()
		val synopsys = ctx["synopsys"]?.fixCase()
		val author = ctx["author"] ?: throw IllegalArgumentException("Author was not provided")
		val subject = ctx["subjects"]?.fixCase()
		val authors = mutableListOf<String>()
		var subjects: MutableList<String>? = mutableListOf()
		println(subject)
		author.split(",").forEach { authors.add(it.trim().fixCase()) }
		subject?.split(",")?.forEach { subjects!!.add(it.trim().fixCase()) }
		
		if (subjects!!.isEmpty()) subjects = null
		BookRepo.add(Book(title, titleLong, isbn, isbn13, deweyDecimal,
			format, publisher, language, datePublished, datePublished,
			edition, pages, binding, dimensions, overview,
			excerpt, synopsys, null, authors, subjects))
		
		
		ctx.html("Book has been added!")
	}
}