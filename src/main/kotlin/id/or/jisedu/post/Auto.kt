package id.or.jisedu.post

import id.or.jisedu.*
import id.or.jisedu.repo.BookRepo
import id.or.jisedu.repo.BookWrapper
import io.javalin.Context
import khttp.get

fun newAuto(ctx: Context, key: String) {
	
	
	
	val isbnLong = ctx["isbn"]
	if (isbnLong == null) {
		ctx.html("Please provide a valid ISBN!")
		return
	}
	var errors = ""
	isbnLong.lines().forEach { it ->
		val status = ctx.errorAwareISBN {
			val isbn = it.replace("-", "")
			if(isbn.isEmpty()) return@errorAwareISBN
			if (!isbn.isValidISBN()) throw IllegalArgumentException("Invalid ISBN format.")
			val json = get("https://api.isbndb.com/book/$isbn", mapOf("X-API-KEY" to key), timeout = 120000.0).text
			println(json)
			val wrapper = Bookshelf.mapper.reader().forType(BookWrapper::class.java).readValue<BookWrapper>(json)
			if(wrapper.book.authors == null) BookRepo.add(wrapper.book.copy(authors = mutableListOf(" ")))
			else BookRepo.add(wrapper.book)
		}
		if (status != null) errors += ("${status}<br>")
		ctx.html("The book has been added!<br>$errors")
	}
}