package id.or.jisedu

import id.or.jisedu.repo.Book
import id.or.jisedu.repo.BookRepo
import id.or.jisedu.user.User
import io.javalin.Context

fun book(ctx: Context) {
	ctx.errorAware {
		val id = ctx.param("id")!!
		
		val book = BookRepo.get(id)
		
		ctx.renderVelocity("/velocity/book/Book.vm", mutableMapOf("book" to book))
	}
}

fun updateAmount(ctx: Context) {
	ctx.errorAware {
		val id = ctx.param("id")!!
		val amount = ctx.param("amount")!!
		val book = BookRepo.get(id)
		book.amount = amount.toInt()
		
		ctx.html("${book.title}'s Amount has been set to $amount")
	}
}

fun new(ctx: Context) = ctx.renderVelocity("/velocity/new/New-Book.vm", mutableMapOf())

fun getAllBooks(ctx: Context, pagesPerBook: Int) {
	ctx.errorAware {
		val auth = ctx.sessionAttribute("auth") ?: false
		val user = if (auth) ctx.sessionAttribute<User?>("currentUser") else null
		val filter = ctx.queryParam("filter") ?: ""
		val type = ctx.queryParam("type") ?: ""
		var page = ctx.queryParam("page")?.toIntOrNull() ?: 1
		
		if (page < 1) page = 1
		var books = when (type) {
			"title"    -> BookRepo.getAll().filter { it.title.containsIgnoreCase(filter) || it.titleLong?.containsIgnoreCase(filter) ?: false }
			"author"   -> BookRepo.getAll().filter { it.authors!!.any { it.containsIgnoreCase(filter) } }
			"subject"  -> BookRepo.getAll().filter { it.subjects?.any { it.containsIgnoreCase(filter) } ?: false }
			"language" -> BookRepo.getAll().filter {
				it.language?.containsIgnoreCase(filter)
					?: "english".containsIgnoreCase("filter")
			}
			else       -> BookRepo.getAll()
		}
		books = books.sortedWith(compareBy({ it.authors!!.first().split(" ").last() }, { it.authors!!.first().split(" ").first() }))
		val tmpBooks = if ((page - 1) * pagesPerBook > books.size) mutableListOf<Book>()
		else books.subList(pagesPerBook * (page - 1), Math.min(pagesPerBook * page, books.size))
		
		
		val model = mutableMapOf("books" to tmpBooks,
			"auth" to auth, "user" to user, "page" to page, "url" to ctx.path())
		
		var path = "/books?"
		if (filter != "") path += "filter=$filter&"
		if (type != "") path += "type=$type&"
		
		if (page > 1) {
			model["prev"] = true
			model["prevpage"] = "${path}page=${page - 1}"
		}
		if (books.size - 1 > page * pagesPerBook - 1) {
			model["next"] = true
			model["nextpage"] = "${path}page=${page + 1}"
		}
		
		ctx.renderVelocity("velocity/book/Books.vm", model)
	}
}