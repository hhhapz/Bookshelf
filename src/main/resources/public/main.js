var squares = document.getElementsByClassName("square");
var newBook = document.getElementById("btn-new");
var newTestBook = document.getElementById("btn-test");

testData = {
    author: "Sunrise, Sunset",
    binding: "30",
    date_published: "12-06-2018",
    dewey_decimal: "DDecimal",
    dimensions: "10x10",
    edition: "1",
    excerpt: "Excerpt",
    format: "Format",
    isbn: "ISBN",
    isbn13: "ISBN-13",
    language: "English",
    overview: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    pages: "20",
    publisher: "Sky",
    subjects: "Thrilerr",
    synopsys: "Synopsys",
    title: "Title",
    title_long: "Title Long"
};

if (newBook != null) {
    newBook.onclick = function () {
        location.href = "/new"
    };
}
if (newTestBook != null) {
    newTestBook.onclick = function () {
        var data = new FormData();

        testData.title += ` #${Math.floor(Math.random() * 99999) + 1}`;

        Object.keys(testData).forEach(function (key) {
            var value = testData[key];
            data.append(key, value);
        });

        const url = "/new-book-manual";

        fetch(url, {
            method: "POST",
            body: data,
        }).then(response => response.text()).then((html) => {
            location.reload()
        })
    };
}
for (var i = 0; i < squares.length; i++) {

    var square = squares[i];
    var content = square.getElementsByClassName("square-content")[0];
    (function (sqr, con) {
        sqr.addEventListener("click", function () {
            console.log(`${con} switched.`);
            con.classList.toggle("active");
        })
    })(square, content)

}